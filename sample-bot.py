import socket
import sys

server = "irc.libera.chat"       #settings
channel = "#tildevarsh"
botnick = "botname"

irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #defines the socket
print("connecting to:"+server)
irc.connect((server, 6667))                                                         #connects to the server
irc.send(bytes("USER "+ botnick +" "+ botnick +" "+ botnick +" :This is a fun bot!\n", 'utf-8')) #user authentication
irc.send(bytes("NICK "+ botnick +"\n", 'utf-8'))                            #sets nick
irc.send(bytes("PRIVMSG nickserv :iNOOPE\r\n", 'utf-8'))    #auth
irc.send(bytes("JOIN "+ channel +"\n", 'utf-8'))        #join the chan

while 1:    #puts it in a loop
   text=irc.recv(2040)  #receive the text
   print(text)   #print text to console

   if text.find(b'PING') != -1:                          #check if 'PING' is found
      irc.send(bytes('PONG ' + text.split()[1].decode() + '\r\n', 'utf-8'))
   if text.find(b':!hi') !=-1: #you can change !hi to whatever you want
      t = text.split(b':!hi') #you can change t and to :)
      to = t[1].strip() #this code is for getting the first word after !hi
      irc.send(bytes('PRIVMSG '+channel+' :Hello '+str(to)+'! \r\n', 'utf-8'))

