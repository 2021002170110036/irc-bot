import socket
import sys
from duckduckgo_search import ddg

server = "irc.libera.chat"       #settings
channel = "#tildevarsh"
botnick = "duckduckgo"

irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #defines the socket
print("connecting to:"+server)
irc.connect((server, 6667))                                                         #connects to the server
irc.send(bytes("USER "+ botnick +" "+ botnick +" "+ botnick +" :This is a fun bot!\n", 'utf-8')) #user authentication
irc.send(bytes("NICK "+ botnick +"\n", 'utf-8'))                            #sets nick
irc.send(bytes("PRIVMSG nickserv :iNOOPE\r\n", 'utf-8'))    #auth
irc.send(bytes("JOIN "+ channel +"\n", 'utf-8'))        #join the chan

while 1:    #puts it in a loop
   text=irc.recv(2040)  #receive the text
   print(text)   #print text to console

   if text.find(b'PING') != -1:                          #check if 'PING' is found
      irc.send(bytes('PONG ' + text.split()[1].decode() + '\r\n', 'utf-8'))
   if text.find(b'!ddg') !=-1: #you can change !hi to whatever you want
      t = text.split(b'!ddg') #you can change t and to :)
      search = t[1].strip() #this code is for getting the first word after !hi
      result = ddg(str(search))
      print(result)
      #irc.send(bytes('PRIVMSG '+channel+' :Hello '+str(to)+'! \r\n', 'utf-8'))
      for r in result[0:2]:
        irc.send(bytes('PRIVMSG '+channel+' :'+'------------------------------------------------'+'\r\n', 'utf-8'))
        irc.send(bytes('PRIVMSG '+channel+' :'+'title:'+r['title']+'\r\n', 'utf-8'))
        irc.send(bytes('PRIVMSG '+channel+' :'+'link:'+r['href']+'\r\n', 'utf-8'))
        irc.send(bytes('PRIVMSG '+channel+' :'+'description:'+r['body']+'\r\n', 'utf-8'))
        irc.send(bytes('PRIVMSG '+channel+' :'+'------------------------------------------------'+'\r\n', 'utf-8'))
      #irc.send(bytes('PRIVMSG '+channel+' :Hello '+'! \r\n', 'utf-8'))

